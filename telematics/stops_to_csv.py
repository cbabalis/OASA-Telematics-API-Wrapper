import string

path = "('10409', ('60299', ('60298', ('60297', ('60296', ('60372', ('61083', ('450010', ('240099', ('240061', ('10433', ('240069', ('240068', ('240067', ('240066', ('240065', ('340084', ('340017', ('340016', ('340103', ('340108', ('340070', ('340069', ('380072', ('380071', ('380070', ('380069', ('380013', ('380012', ('380011', ('380010', ('380102', ('380101', ('380100', ('380099', ('380098', ('40087', ('40086', ('40085', ('10349', ())))))))))))))))))))))))))))))))))))))))"

# convert the path to a python list and write it to a file.
chars_to_remove = ['\'', '(', ')']
for c in chars_to_remove:
    path = path.replace(c, '')

# now create a list of the stops
path = path.replace('  ', '')
stops = path.split(',')

# finally write it to a file
with open('stops.csv', 'w') as f:
    for s in stops:
        f.write(s)
        f.write('\n')

print(stops)
