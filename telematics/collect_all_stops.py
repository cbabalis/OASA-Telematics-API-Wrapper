import telematics


# first of all, we need to get the lines.
lines = telematics.webGetLines()

# then, we get the routes of the lines.
routes = []
for l in lines:
    # l[0] is the key based on which we find the routes
    routes.append(telematics.webGetRoutes(l[0]))
    print("another line has been printed", l[0])

    with open('routes.txt', 'w') as f:
        for r in routes:
            f.write(str(r[0]))
            f.write('\n')

# then, we can safely extract the stops each route has.
stops = []
for r in routes:
    if r:
        stops.append(telematics.webGetStops(r[1][0]))
        print("r now is ", r[1][0])
        with open('all_stops.csv', 'a') as f:
            for s in stops:
                print("another stop has been printed", s[0])
                f.write("%s\n" % s)
# finally, we create a new list of dictionaries. Key of each dictionary
# is a stop and the values, are the stops with which the specific stop
# connects.
