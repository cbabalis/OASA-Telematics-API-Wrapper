from typing import Dict, List

import requests


# Most of the time it returns List[Dict], But it also happens to Return a Dict.
def telematics_request(query: str):
    api_endpoint = 'http://telematics.oasa.gr/api/'

    req = requests.post(api_endpoint + query)

    if req.text == 'null':
        # even though server returns null,
        # IT STILL RETURNS A 200 STATUS CODE FOR SOME REASON.
        req.status_code = 404

    try:
        req.raise_for_status()

    except (requests.exceptions.HTTPError,
            requests.exceptions.ConnectionError,
            requests.exceptions.Timeout):

        # everything else messes up with the iterators
        return []

    # theres a better way probably
    # if req.status_code == requests.codes.ok:
    #     return req.json()

    return req.json()


def webGetLines() -> List[Dict]:
    query = '?act=webGetLines'
    return telematics_request(query)


def webGetLinesWithMLInfo() -> List[Dict]:
    query = '?act=webGetLinesWithMLinfo'
    return telematics_request(query)


def webGetRoutes(linecode: int) -> List[Dict]:
    query = f'?act=webGetRoutes&p1={linecode}'
    return telematics_request(query)


def webRouteDetails(routecode: int) -> List[Dict]:
    query = f'?act=webRouteDetails&p1={routecode}'
    return telematics_request(query)


def webGetStops(routecode: int) -> List[Dict]:
    query = f'?act=webGetStops&p1={routecode}'
    return telematics_request(query)


def webRoutesForStop(stopcode: int) -> List[Dict]:
    query = f'?act=webRoutesForStop&p1={stopcode}'
    return telematics_request(query)


def webGetRoutesDetailsAndStops(routecode: int) -> Dict:
    query = f'?act=webGetRoutesDetailsAndStops&p1={routecode}'
    return telematics_request(query)


def getStopArrivals(stopcode: int) -> List[Dict]:
    query = f'?act=getStopArrivals&p1={stopcode}'
    return telematics_request(query)


def getBusLocation(routecode: int) -> List[Dict]:
    query = f'?act=getBusLocation&p1={routecode}'
    return telematics_request(query)


def getScheduleDaysMasterline(linecode: int) -> List[Dict]:
    query = f'?act=getScheduleDaysMasterline&p1={linecode}'
    return telematics_request(query)


def getLinesAndRoutesForMl(mlcode: int) -> List[Dict]:
    query = f'?act=getLinesAndRoutesForMl&p1={mlcode}'
    return telematics_request(query)


def getRoutesForLine(linecode: int) -> List[Dict]:
    query = f'?act=getRoutesForLine&p1={linecode}'
    return telematics_request(query)


def getMLName(mlcode: int) -> List[Dict]:
    query = f'?act=getMLName&p1={mlcode}'
    return telematics_request(query)


def getLineName(linecode: int) -> List[Dict]:
    query = f'?act=getLineName&p1={linecode}'
    return telematics_request(query)


def getRouteName(routecode: int) -> List[Dict]:
    query = f'?act=getRouteName&p1={routecode}'
    return telematics_request(query)


def getStopNameAndXY(stopcode: int) -> List[Dict]:
    query = f'?act=getStopNameAndXY&p1={stopcode}'
    return telematics_request(query)


def getSchedLines(mlcode: int, linecode: int, sdc_code: int) -> Dict:
    query = f'?act=getddSchedLines&p1={mlcode}&p2={sdc_code}&p3={linecode}'
    return telematics_request(query)


def getClosestStops(long: float, lat: float) -> List[Dict]:
    query = f'?act=getClosestStops&p1={long}&p2={lat}'
    return telematics_request(query)
