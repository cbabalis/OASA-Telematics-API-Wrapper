""" To script douleuei ws eksis:
Xrisimopoiwntas to etoimo API apo telematics, tha prospathisoume na
anaktisoume tis staseis (stops). O algorithmos exei ws eksis:

1. Pairnoume tis diadromes (routes).
2. Apo ta anaktithenta dedomena, pairnoume ta route ids.
3. me vasi to route id psaxnoume ta stops
4. anaktoume ola ta stops.

TODO:
5. epeksergazomaste tis staseis kathe diadromis kai gia kathe stasi:
    6. thewroume tin epomeni tis ws syndesmo se autin.
    7. ypologizoume to kostos (aploika) pairnontas tin diafora twn syntetagmenwn
    8. ta vazoume se ena syntheto dictionary <key:value> opou:
        - key: to onoma tis stasis
        - value: mia lista apo dictionaries, opou:
            * key: to onoma tis stasis pou syndeetai me tin proigoumeni
            * value: i apostasi pou vrikame proigoumenws.
"""

import telematics

lines = telematics.webGetLines()

# get the routes from OASA API
routes = []
for l in lines:
    routes.append(telematics.webGetRoutes(l[0]))
    with open('routes.txt', 'w') as f:
        for r in routes:
            f.write(str(r[0]))
            f.write('\n')

# Step 2: Take the route ids
route_code = []

for r in routes:
    r = r.split(",")
    rc = r[0].split("=")[-1]
    route_code.append(rc)

# Step 4: Take all stops
stops = []

for rc in route_code:
    try:
        x = telematics.webGetStops(rc)
        stops.append(x)
    except:
        continue #TODO: 2 stops are missing. OASA needs some rework
