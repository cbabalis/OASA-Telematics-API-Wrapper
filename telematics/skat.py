import telematics
import pdb

lines = telematics.webGetLines()

routes = []

with open('routes.txt', 'r') as f:
    x = f.readlines()
    for i in x:
        routes.append(i)

# routes now contain all downloaded data
line_code = []
for r in routes:
    r = r.split(",")
    lc = r[0].split("=")[-1]
    line_code.append(lc)

# now collect the stops
stops = []

i = 0
pdb.set_trace()
#for i in range(0, 217):
# TODO route_code not line code
for lc in line_code:
    try:
        x = telematics.webGetStops(lc)
        print(i)
        i = i + 1
        stops.append(telematics.webGetStops(lc))
    except:
        continue

print("we 're good!")
pdb.set_trace()

with open('stops.txt', 'w') as f:
    for s in stops:
        f.write(str(s))
        f.write('\n')

