import pdb

def get_stop_code(i, stops):
    if stops:
        stop_elems = stops[i].split(",")
        return stop_elems[0].split("=")[-1]

def get_cost(i, stops):
    current_lat = float(get_element(i, stops, elem=7))
    current_long = float(get_element(i, stops, elem=8))
    next_lat = float(get_element(i+1, stops, elem=7))
    next_long = float(get_element(i+1, stops, elem=8))
    cost = compute_cost(current_lat, current_long, next_lat, next_long)
    return cost

def get_element(i, stops, elem):
    if stops:
        try:
            stop_elems = stops[i].split(",")
            return stop_elems[elem].split("=")[-1]
        except:
            # if anything goes wrong, just return a really big number
            # (so as not to be useful in a dijkstra)
            print("Error for elems!")
            return 100000000

def compute_cost(curr_lat, curr_long, next_lat, next_long):
    cost = ((curr_lat - next_lat)**2 + (curr_long - next_long)**2)**(0.5)
    return cost


def populate_stop_connections(adjacency_matrix, stop_code, next_code, cost):
    if stop_code in adjacency_matrix.keys():
        # should be a dictionary too
        existing_values = adjacency_matrix[stop_code]
        existing_values[next_code] = cost
        adjacency_matrix[stop_code] = existing_values
    else:
        adjacency_matrix[stop_code] = {next_code:cost}


# load the file
with open('stops.txt', 'r') as f:
    routes = f.readlines()

stops = []

for r in routes:
    stops.extend(r.split("Stop("))
    print("stops are ", len(stops))

adjacency_matrix = {}
for i in range(1, len(stops)-1):
    # retrieve the code of the current stop
    stop_code = get_stop_code(i, stops)
    # retrieve the code of the next stop (in order to link the stops)
    next_code = get_stop_code(i+1, stops)
    print("stop code is {} and next_code is {}".format(stop_code, next_code))
    cost = get_cost(i, stops)
    # create the dictionary of <node: cost-to-go-there-value>
    populate_stop_connections(adjacency_matrix, stop_code, next_code, cost)

with open('adj_matrix.txt', 'w') as f:
    for entry in adjacency_matrix:
        print("adjacency matrix key is ", entry)
        f.write(str(entry))
        f.write(":")
        f.write(str(adjacency_matrix[entry]))
        f.write('\n')

# data analysis: compute percentage of more than one ascendants
counter = 0
big_counter = 0
for entry in adjacency_matrix:
    if len(adjacency_matrix[entry]) > 1:
        if len(adjacency_matrix[entry]) > 2:
            big_counter += 1
        counter += 1

total_sum = counter / len(adjacency_matrix)
print("total sum is ", total_sum)

sum_2 = big_counter / counter
print("more than 2 stops to more than 1 stops are ", sum_2)
