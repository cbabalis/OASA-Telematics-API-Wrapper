# OASA-Telematics-Wrapper
## Python wrapper for the Telematics API


Based on the documentation provided by [OASA-Telematics-API-Documentation](https://github.com/gph03n1x/OASA-Telematics-API-Documentation) and v 0.1.1 of [this script](http://telematics.oasa.gr/js/script.js)

## WIP. INITIAL HACKY IMPLEMENTATION


Mapped every documented function to a NamedTuple python object.
There is also raw mapping available in src/raw_api.py that returns raw requests objects

If you are not familiar with NamedTuples check [this](https://mypy.readthedocs.io/en/stable/kinds_of_types.html#named-tuples) and [this](https://docs.python.org/2/library/collections.html#collections.namedtuple)


### Dependencies:


* Requires Python 3.6+.
* [Requests](https://github.com/requests/requests)

### Usage:


```
pip install telematics
python
>>> import telematics
>>> masterlines = telematics.webGetLines()
>>> [print(i) for i in masterlines]
Line(line_code=1151, line_id='021', line_descr='ΠΛΑΤΕΙΑ ΚΑΝΙΓΓΟΣ - ΓΚΥΖH(ΚΥΚΛΙΚΗ)', line_descr_eng='PLATEIA KANIGKOS - GKIZI')
Line(line_code=821, line_id='022', line_descr='Ν. ΚΥΨΕΛΗ - ΜΑΡΑΣΛΕΙΟΣ', line_descr_eng='NEA KYPSELI - MARASLEIOS')
Line(line_code=750, line_id='024', line_descr='ΑΓ. ΑΝΑΡΓΥΡΟΙ - ΣΤ. ΚΑΤΩ ΠΑΤΗΣΙΑ', line_descr_eng='AG. ANARGYROI - ST. KATO PATHSIA')
Line(line_code=817, line_id='025', line_descr='ΙΠΠΟΚΡΑΤΟΥΣ - ΠΡΟΦΗΤΗ ΔΑΝΙΗΛ', line_descr_eng='IPPOKRATOUS - PROFITI DANIIL')
Line(line_code=818, line_id='026', line_descr='ΙΠΠΟΚΡΑΤΟΥΣ - ΒΟΤΑΝΙΚΟΣ', line_descr_eng='IPPOKRATOUS - VOTANIKOS')
Line(line_code=974, line_id='027', line_descr='ΙΠΠΟΚΡΑΤΟΥΣ - ΟΡΦΕΩΣ', line_descr_eng='IPPOKRATOUS - ORFEOS')
Line(line_code=816, line_id='035', line_descr='ΑΝΩ ΚΥΨΕΛΗ - ΠΕΤΡΑΛΩΝΑ - ΤΑΥΡΟΣ', line_descr_eng='ANO KYPSELI - PETRALONA - TAVROS')
Line(line_code=804, line_id='036', line_descr='ΣΤ. ΚΑΤΕΧΑΚΗ -ΣΤ. ΠΑΝΟΡΜΟΥ-ΓΑΛΑΤΣΙ-ΚΥΨΕΛΗ (ΚΥΚΛΙΚΗ)', line_descr_eng='KATECHAKI - KYPSELI')
Line(line_code=938, line_id='040', line_descr='ΠΕΙΡΑΙΑΣ - ΣΥΝΤΑΓΜΑ', line_descr_eng='PEIRAIAS - SINTAGMA')
Line(line_code=831, line_id='046', line_descr='ΜΟΥΣΕΙΟ - ΕΛΛΗΝΟΡΩΣΩΝ', line_descr_eng='MOUSEIO - ELLINOROSON')
Line(line_code=819, line_id='049', line_descr='ΠΕΙΡΑΙΑΣ - ΟΜΟΝΟΙΑ', line_descr_eng='PEIRAIAS - OMONOIA')
...
```

### Local Build:


```
git clone https://github.com/alatiera/OASA-Telematics-API-Wrapper.git
cd OASA-Telematics-API-Wrapper
python3 -mpip install .
```


### TODO:

- [x] Add Setup file
- [x] Add Usage Instructions
- [ ] Improve the Documentation
- [x] Refactor the way requests are made
- [x] Catch exceptions on from bad data(kinda)
